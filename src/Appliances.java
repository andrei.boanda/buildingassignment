public enum Appliances {
    COFFEE_MACHINE("Coffee_machine(s)"),
    WATER_DISPENSER("Water dispenser(s)"),
    FRIDGE("Fridge"),
    TV("TV"),
    VIDEO_PROJECTOR("Video projector(s)"),
    TELEPRESENCE("Telepresence device(s)");

    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    Appliances(String val) {
        this.value = val;
    }

    public String getValue() {
        return value;
    }

}
