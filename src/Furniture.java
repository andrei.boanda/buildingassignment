public enum Furniture
{
    DESK("Desk(s)"),
    SEAT("Seat(s)");

    private String value;

    Furniture(String val) {

        this.value = val;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
