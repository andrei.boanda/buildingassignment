import java.util.*;

public class Room {

    Room() {
        this.roomName = "UNKNOWN_ROOM_NAME";
        for(Appliances a : Appliances.values())
        {
            appliances.put(a, 0);
        }

        for(Furniture f : Furniture.values())
        {
            furniture.put(f, 0);
        }
    }

    private RoomType roomType;
    private String roomName;
    private HashMap<Appliances, Integer> appliances = new HashMap<Appliances, Integer>();
    private HashMap<Furniture, Integer> furniture = new HashMap<Furniture, Integer>();


    void addAppliance(Appliances type, int howMany) {
        int existingApplianceNo = appliances.get(type);
        existingApplianceNo = existingApplianceNo + howMany;
        appliances.put(type, existingApplianceNo);
    }

    void addFurniture(Furniture type, int howMany) {
        int existingFurnitureNo = furniture.get(type);
        existingFurnitureNo = existingFurnitureNo + howMany;
        furniture.put(type, existingFurnitureNo);
    }

    String getRoomName() {
        return roomName;
    }

    void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    RoomType getRoomType() {
        return roomType;
    }

    void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public String toString() {
        String s = this.roomName;

        for(Furniture furnitureType : furniture.keySet())
        {
            int existingFurnitureNo = furniture.get(furnitureType);
            if(existingFurnitureNo > 0)
            {
                s = s + "\n\t\t\t\t" + existingFurnitureNo + " " + furnitureType;
            }
        }

        for(Appliances applianceType : appliances.keySet())
        {
            int existingAppliancesNo = appliances.get(applianceType);
            if(existingAppliancesNo > 0)
            {
                s = s + "\n\t\t\t\t" + existingAppliancesNo + " " + applianceType.getValue();
            }
        }

        return s;
    }
}
